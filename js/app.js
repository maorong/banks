$(function(){
	var $province = $('#province'),
		$city = $('#city'),
		$district = $('#district');
	var $banks = $('#banks');
	
	$province.on('change', function(){
		var val = $(this).val();
		if(val !== ''){
			initCity();
			$district.html('<option value="">请选择</option>');
		}else{
			$city.html('<option value="">请选择</option>');
			$district.html('<option value="">请选择</option>');
		}
		$banks.empty();
	})

	$city.on('change', function(){
		var val = $(this).val();
		if(val !== ''){
			initDistrict();
		}else{
			$district.html('<option value="">请选择</option>');
		}
		$banks.empty();
	})

	$district.on('change', function(){
		var val = $(this).val();
		if(val !== ''){
			getBanks();
		}else{
			$banks.empty();
		}
	})

	initProvince();

	function initProvince(){
		var provinceHTML = '<option value="">请选择</option>';
		$.each(bankList, function(index, element){
			provinceHTML+='<option value='+bankList[index].name+'>'+bankList[index].name+'</option>';
		})
		$province.html(provinceHTML);
	}

	function initCity(){
		var idx = $province.get(0).selectedIndex;
		if(idx !== 0){
			var cityHTML = '<option value="">请选择</option>';
			var arr = bankList[idx-1].city;
			$.each(arr, function(index, element){
				cityHTML += '<option value='+arr[index].name+'>'+arr[index].name+'</option>';
			})
			$city.html(cityHTML);
		}
	}

	function initDistrict(){
		var provinceIdx = $province.get(0).selectedIndex;
		var cityIdx = $city.get(0).selectedIndex;
		if(cityIdx != 0){
			var districtHTML = '<option value="">请选择</option>';
			var arr = bankList[provinceIdx-1].city[cityIdx-1].area;
			$.each(arr, function(index, element){
				districtHTML += '<option value='+arr[index].name+'>'+arr[index].name+'</option>'
			})
			$district.html(districtHTML);
		}
	}


	function getBanks(){
		var provinceIdx = $province.get(0).selectedIndex;
		var cityIdx = $city.get(0).selectedIndex;
		var districtIdx = $district.get(0).selectedIndex;
		if(districtIdx != 0){
			var banksHTML = '';
			var arr = bankList[provinceIdx-1].city[cityIdx-1].area[districtIdx-1].list;
			$.each(arr, function(index, element){
				banksHTML += '<li><h3>'+arr[index].name+'</h3><p>'+arr[index].address+'</p><p>'+arr[index].tel+'</p></li>';
			})
			$banks.html(banksHTML);
		}
	}


	//定位设置当前区域
	function setArea(province, city, district){
		$.each(bankList, function(index, element){
			if(element.name == province){
				$province.val(province);
				initCity();
				var cityList = element.city;
				$.each(cityList, function(cityIndex, cityElement){
					if(cityElement.name == city){
						$city.val(city);
						initDistrict();
						var districtList = cityElement.area;
						$.each(districtList, function(districtIndex, districtElement){
							if(districtElement.name == district){
								$district.val(district);
								getBanks();
							}
						})
					}
				})
			}
		})
	}


	function setCity(val){
		var arr = bankList.
		$.each(bankList, function(index, element){
			if(element.name == val){
				$province.val(val);
			}
		})
	}


	getLocation();

	function getLocation(){
		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(showPosition, showError);
		}else{
			alert('浏览器不支持地理定位');
		}
	}

	function showError(error){ 
	  switch(error.code) { 
	    case error.PERMISSION_DENIED: 
	      alert("定位失败,用户拒绝请求地理定位"); 
	      break; 
	    case error.POSITION_UNAVAILABLE: 
	      alert("定位失败,位置信息是不可用"); 
	      break; 
	    case error.TIMEOUT: 
	      alert("定位失败,请求获取用户位置超时"); 
	      break; 
	    case error.UNKNOWN_ERROR: 
	      alert("定位失败,定位系统失效"); 
	      break; 
	  } 
	}
	function showPosition(position){
		var lat = position.coords.latitude; //纬度 
	  	var lng = position.coords.longitude; //经度
	  	var url = 'https://api.map.baidu.com/geocoder/v2/?callback=renderReverse&location='+lat+','+lng+'&output=json&pois=1&ak=cvtFOLBFXuxx5yFna9ta1MNdCNZbqxpo'
	  	$.ajax({
	  		url: url,
	  		type: 'get',
	  		dataType: 'jsonp',
	  		success: function(r){
	  			if(r.status==0){
	  				setArea(r.result.addressComponent.province, r.result.addressComponent.city, r.result.addressComponent.district);
				} 
	  		}
	  	})
	}
})